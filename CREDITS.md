# Credits
* Seal picture used in header image: ["Seals at Nantucket National Wildlife Refuge"](https://www.flickr.com/photos/43322816@N08/5961318915) by [U. S. Fish and Wildlife Service - Northeast Region](https://www.flickr.com/photos/43322816@N08) is marked with [CC PDM 1.0](https://creativecommons.org/publicdomain/mark/1.0/?ref=ccsearch&atype=rich)
