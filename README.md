# How to use Podman on Devuan Linux

![collage of cute seals + neofetch rendering of Devuan Linux logo](/assets/seals-devuan.png)

## Instructions
* Open a terminal
* Run `sudo apt update && sudo apt install podman -y`
* Use Podman as you normally would on any computer with systemd
* Take note of the troubleshooting guides below if you encounter the associated errors

## Troubleshooting
**If running a simple command like `podman search debian` returns the following output:**
```
WARN[0000] The cgroupv2 manager is set to systemd but there is no systemd user session available 
WARN[0000] For using systemd, you may need to login using an user session 
WARN[0000] Alternatively, you can enable lingering with: `loginctl enable-linger 1000` (possibly as root) 
WARN[0000] Falling back to --cgroup-manager=cgroupfs    
WARN[0000] The cgroupv2 manager is set to systemd but there is no systemd user session available 
WARN[0000] For using systemd, you may need to login using an user session 
WARN[0000] Alternatively, you can enable lingering with: `loginctl enable-linger 1000` (possibly as root) 
WARN[0000] Falling back to --cgroup-manager=cgroupfs    
```
* Create or append `/etc/containers/containers.conf` with the following content to explicitly set the cgroup manager to `cgroupfs` rather than the default `systemd`:
    ```
    [engine]
    cgroup_manager = "cgroupfs"
    ```

**If `podman run [name-of-container]` returns the following output:**
```
ERRO[0000] unable to write pod event: "write unixgram @00073->/run/systemd/journal/socket: sendmsg: no such file or directory" 
ERRO[0000] unable to write pod event: "write unixgram @00073->/run/systemd/journal/socket: sendmsg: no such file or directory" 
ERRO[0000] unable to write pod event: "write unixgram @00073->/run/systemd/journal/socket: sendmsg: no such file or directory" 
```
* Create or append `/etc/containers/containers.conf` with the following content to log to a file rather than the systemd journal:
```
[engine]
events_logger = "file"
```
